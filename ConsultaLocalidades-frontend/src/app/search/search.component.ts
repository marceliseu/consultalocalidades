import { Component, OnInit, ViewChild} from '@angular/core';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of, Subject, merge} from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, filter, map, tap, switchMap} from 'rxjs/operators';
import { SearchService } from './search.service';
import { Estado } from "./estado.model";
import { Regiao } from "./regiao.model";
import { FormGroup, FormBuilder } from '@angular/forms';
import { Municipio } from './municipio.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
    
    estados: Estado[];
    municipios: Municipio[];
    model: any;
    model2: any;

    constructor(private searchService: SearchService, private formBuilder: FormBuilder,) {
        }

    ngOnInit() {
        this.searchService.ListEstados().subscribe(data => this.estados = data);
        this.municipios = [];
    }

    onBlur(){ 
        this.model2 = '';
        this.municipios = [];
        if (typeof this.model != 'undefined' && this.model) {
             if ( (typeof this.model.id != 'undefined' && this.model.id) ){
                this.searchService.ListCidades(this.model.id).subscribe(data => this.municipios = data);
             }
        }
       }


    @ViewChild('instance', {static: true}) instance: NgbTypeahead;
    focus$ = new Subject<string>();
    click$ = new Subject<string>();
    focus2$ = new Subject<string>();
    click2$ = new Subject<string>();

    search1 = (text$: Observable<string>) => {
        const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
        const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
        const inputFocus$ = this.focus$;
    
        return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
          map(term => (term === '' ? this.estados
            : this.estados.filter(v => v.nome.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
        );
      }
  
    search2 = (text$: Observable<string>) => {
      const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
      const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
      const inputFocus$ = this.focus2$;
  
      return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
        map(term => (term === '' ? this.municipios
          : this.municipios.filter(v => v.nome.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
      );
    }

    formatter = (x: {nome: string}) => x.nome;
}
