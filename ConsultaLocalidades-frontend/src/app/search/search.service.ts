
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Estado } from './estado.model';
import { Municipio } from './municipio.model';

@Injectable({
  providedIn: 'root'
})
export class SearchService {


   // Base url
   baseurl = 'http://localhost:8080';


   constructor(private http: HttpClient) { }


  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

    // Lista Estados
    ListEstados(): Observable<Estado[]> {
      return this.http.get<Estado[]>(this.baseurl + '/estados')
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      )
    }

    // Lista Cidades
    ListCidades(UFid: string): Observable<Municipio[]> {
      return this.http.get<Municipio[]>(this.baseurl + '/cidades/' + UFid)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      )
    }


    // Lista Cidades
    ListCidades2(UFid: string): Observable<string[]> {
      return this.http.get<string[]>(this.baseurl + '/cidades2/' + UFid)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      )
    }    

     // Error handling
   errorHandl(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
 }

 
}

