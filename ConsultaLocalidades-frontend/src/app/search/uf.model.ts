import { Regiao } from "./regiao.model";

export class UF {

    id: number;
    sigla: string;
    nome: string
    regiao: Regiao;
  
    constructor() {

    }
  
  }