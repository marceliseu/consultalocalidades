import { Microrregiao } from "./microregiao.model";

export class Municipio {

    id: number;
    nome: string
    microregiao: Microrregiao;
  
    constructor() {

    }
  
  }