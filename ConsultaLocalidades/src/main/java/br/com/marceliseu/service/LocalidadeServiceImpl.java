package br.com.marceliseu.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

import br.com.marceliseu.model.Estado;
import br.com.marceliseu.model.Municipio;

@Service
public class LocalidadeServiceImpl implements LocalidadeService {

	Logger logger = LoggerFactory.getLogger(LocalidadeServiceImpl.class);

    @Autowired
    private RestTemplate restTemplate;
    
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
       return builder.build();
    }
    
	@Override
	@Cacheable("estados")
	@HystrixCommand(fallbackMethod = "listEstados_Fallback",
    commandProperties = {
       @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "10000"),
       @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value="60")
	})
	public List<Estado> listEstados() {
    	List<Estado> estados = null;
        String urlStr = "https://servicodados.ibge.gov.br/api/v1/localidades/estados";
        try {       
        	 logger.info("Chamando API para listar Estados" );
         	 ResponseEntity<List<Estado>> response = restTemplate.exchange(
           			 urlStr,
           	   HttpMethod.GET,
           	   null,
           	   new ParameterizedTypeReference<List<Estado>>(){});
           	   estados = response.getBody();
        } catch (RestClientException e) {
        	logger.error("Erro chamada API:" + e.getMessage());
        }     	
    	return estados;
	}

	public List<Estado> listEstados_Fallback() {
		logger.info("Executando em modo de CircuitBreaker - lista Estados" );
		List<Estado> estados =  new ArrayList<Estado>();
		return estados;
	}

	@Override
	@Cacheable("municipios")
	@HystrixCommand(fallbackMethod = "listMunicipios_Fallback",
    commandProperties = {
       @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "10000"),
       @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value="60")
	})
	public List<Municipio> listMunicipios(String UF_id) {		
		List<Municipio> municipios = null;
        String urlStr = "https://servicodados.ibge.gov.br/api/v1/localidades/estados/"
        		+ UF_id
        		+ "/municipios";  
        try {   
			 logger.info("Chamando API para listar Municipios UF:" + UF_id );
			 ResponseEntity<List<Municipio>> response =  restTemplate.exchange(
					 urlStr,
	       	   HttpMethod.GET,
	       	   null,
			   new ParameterizedTypeReference<List<Municipio>>(){});
			 municipios = response.getBody();
			 logger.debug("Qtdade de municipios:" + municipios.size());  
        } catch (RestClientException e) {
        	logger.error("Erro chamada API:" + e.getMessage());
        }     	
		return municipios;
	}
	
	public List<Municipio> listMunicipios_Fallback(String UF_id) {	
		logger.info("Executando em modo de CircuitBreaker - lista Municipios UF:" + UF_id );
		List<Municipio> municipios =  new ArrayList<Municipio>();
		return municipios;
	}

	@Override
	@Cacheable("cidades")
	@HystrixCommand(fallbackMethod = "getMunicipio_Fallback",
    commandProperties = {
       @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "10000"),
       @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value="60")
	})
	public Municipio getMunicipio(String nomeCidade) {
		Municipio municipio = null;
        String urlStr = "https://servicodados.ibge.gov.br/api/v1/localidades/municipios/"
        		+ nomeCidade ;
        try { 
        	logger.info("Chamando API para obter dados do municipio " + nomeCidade);
        	municipio  = restTemplate.getForObject(urlStr, Municipio.class);
        } catch (RestClientException e) {
            logger.error("Erro chamada API:" + e.getMessage());
        }   		
		return municipio;
	}
	
	public Municipio getMunicipio_Fallback(String nomeCidade) {
		Municipio municipio = null;
		logger.info("Executando em modo de CircuitBreaker - obtendo dados Municipio:" + nomeCidade);
		return municipio;
	}	

}
