package br.com.marceliseu.service;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;

import br.com.marceliseu.model.Estado;
import br.com.marceliseu.model.Municipio;

public interface LocalidadeService {

	List<Estado> listEstados();
	List<Municipio> listMunicipios(String UF_id);
	Municipio getMunicipio(@PathVariable String nomeCidade);



}
