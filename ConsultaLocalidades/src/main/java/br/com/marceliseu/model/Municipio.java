
package br.com.marceliseu.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Municipio implements Serializable {

private Integer id;
private String nome;
private Microrregiao microrregiao;
private static final long serialVersionUID = 1L;

public Municipio() {
}

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getNome() {
return nome;
}

public void setNome(String nome) {
this.nome = nome;
}

public Microrregiao getMicrorregiao() {
return microrregiao;
}

public void setMicrorregiao(Microrregiao microrregiao) {
this.microrregiao = microrregiao;
}

}