package br.com.marceliseu.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Regiao implements Serializable {


private long id;
private String sigla;
private String nome;
private static final long serialVersionUID = 1L;

public Regiao() {
}

public long getId() {
return id;
}

public void setId(long id) {
this.id = id;
}

public String getSigla() {
return sigla;
}

public void setSigla(String sigla) {
this.sigla = sigla;
}

public String getNome() {
return nome;
}

public void setNome(String nome) {
this.nome = nome;
}

}