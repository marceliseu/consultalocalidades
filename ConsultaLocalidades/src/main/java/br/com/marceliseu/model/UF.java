package br.com.marceliseu.model;

import java.io.Serializable;

public class UF implements Serializable
{

private int id;
private String sigla;
private String nome;
private Regiao regiao;
private final static long serialVersionUID = -2300422844979721217L;

/**
* No args constructor for use in serialization
*
*/
public UF() {
}

/**
*
* @param id
* @param sigla
* @param nome
* @param regiao
*/
public UF(int id, String sigla, String nome, Regiao regiao) {
super();
this.id = id;
this.sigla = sigla;
this.nome = nome;
this.regiao = regiao;
}

public int getId() {
return id;
}

public void setId(int id) {
this.id = id;
}

public String getSigla() {
return sigla;
}

public void setSigla(String sigla) {
this.sigla = sigla;
}

public String getNome() {
return nome;
}

public void setNome(String nome) {
this.nome = nome;
}

public Regiao getRegiao() {
return regiao;
}

public void setRegiao(Regiao regiao) {
this.regiao = regiao;
}
}