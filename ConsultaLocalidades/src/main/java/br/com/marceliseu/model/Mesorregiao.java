
package br.com.marceliseu.model;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"nome",
"UF"
})
public class Mesorregiao implements Serializable
{

@JsonProperty("id")
private int id;
@JsonProperty("nome")
private String nome;
@JsonProperty("UF")
private UF uF;
private final static long serialVersionUID = 6265374394608772172L;

/**
* No args constructor for use in serialization
*
*/
public Mesorregiao() {
}

/**
*
* @param id
* @param uF
* @param nome
*/
public Mesorregiao(int id, String nome, UF uF) {
super();
this.id = id;
this.nome = nome;
this.uF = uF;
}

public int getId() {
return id;
}

public void setId(int id) {
this.id = id;
}

public String getNome() {
return nome;
}

public void setNome(String nome) {
this.nome = nome;
}

public UF getUF() {
return uF;
}

public void setUF(UF uF) {
this.uF = uF;
}

}