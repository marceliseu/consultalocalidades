package br.com.marceliseu.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CidadeList {

	private List<Cidade> cidades = new ArrayList<Cidade>();

	public CidadeList() {
	}
	
	public List<Cidade> getCidades() {
		return cidades;
	}

	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}
	
	public void addCidades(Cidade cidade) {
		cidades.add(cidade);
	}
	
}
