package br.com.marceliseu.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Microrregiao implements Serializable {

private long id;
private String nome;
private Mesorregiao mesorregiao;
private static final long serialVersionUID = 1L;

public Microrregiao() {
}

public long getId() {
return id;
}

public void setId(long id) {
this.id = id;
}

public String getNome() {
return nome;
}

public void setNome(String nome) {
this.nome = nome;
}

public Mesorregiao getMesorregiao() {
return mesorregiao;
}

public void setMesorregiao(Mesorregiao mesorregiao) {
this.mesorregiao = mesorregiao;
}

}