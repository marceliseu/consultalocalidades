package br.com.marceliseu.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Estado implements Serializable {

private Integer id;
private String sigla;
private String nome;
private Regiao regiao;
private static final long serialVersionUID = 1L;

public Estado() {
}

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getSigla() {
return sigla;
}

public void setSigla(String sigla) {
this.sigla = sigla;
}

public String getNome() {
return nome;
}

public void setNome(String nome) {
this.nome = nome;
}

public Regiao getRegiao() {
return regiao;
}

public void setRegiao(Regiao regiao) {
this.regiao = regiao;
}

}