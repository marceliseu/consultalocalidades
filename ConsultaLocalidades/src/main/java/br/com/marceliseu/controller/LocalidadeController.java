package br.com.marceliseu.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import br.com.marceliseu.model.Cidade;
import br.com.marceliseu.model.CidadeID;
import br.com.marceliseu.model.CidadeList;
import br.com.marceliseu.model.Estado;
import br.com.marceliseu.model.Mesorregiao;
import br.com.marceliseu.model.Municipio;
import br.com.marceliseu.service.LocalidadeService;

@CrossOrigin(maxAge = 3600)
@RestController
public class LocalidadeController {
	
	Logger logger = LoggerFactory.getLogger(LocalidadeController.class);
	
	@Autowired	
	private LocalidadeService localidadeService;

    // Lista Cidades
    @GetMapping(path = "/cidades", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public List<Cidade>  listCidades() {    
		CidadeList cidades = new CidadeList();
    	List<Estado> estados = localidadeService.listEstados();
    	
       	estados.forEach((estado) -> {
       		logger.debug("Estado:" + estado.getNome());
       		List<Municipio> municipios = localidadeService.listMunicipios(estado.getId().toString());
       		municipios.forEach((municipio) -> {
           		Cidade cidade = new Cidade();
           		cidade.setIdEstado(estado.getId());
           		cidade.setSiglaEstado(estado.getSigla());
           		cidade.setRegiaoNome(estado.getNome());
       			cidade.setNomeCidade(municipio.getNome());
       			Mesorregiao mesoregiao = municipio.getMicrorregiao().getMesorregiao();
       			cidade.setNomeMesorregiao(mesoregiao.getNome());
       			cidade.setNomeFormatado(cidade.getNomeCidade() + "/" + cidade.getSiglaEstado());
       			cidades.addCidades(cidade);
       		}); 
       		
       	});     	
		return cidades.getCidades();
    }    
    
    // Lista Cidades CSV
    @GetMapping(path = "/cidadesCSV", produces = "text/csv; charset=UTF-8")
    @ResponseBody
    public OutputStream  listCidadesCSV(HttpServletResponse response) {    
		CidadeList cidades = new CidadeList();
    	List<Estado> estados = localidadeService.listEstados();
    	
       	estados.forEach((estado) -> {
       		logger.debug("Estado:" + estado.getNome());
       		List<Municipio> municipios = localidadeService.listMunicipios(estado.getId().toString());
       		municipios.forEach((municipio) -> {
           		Cidade cidade = new Cidade();
           		cidade.setIdEstado(estado.getId());
           		cidade.setSiglaEstado(estado.getSigla());
           		cidade.setRegiaoNome(estado.getNome());
       			cidade.setNomeCidade(municipio.getNome());
       			Mesorregiao mesoregiao = municipio.getMicrorregiao().getMesorregiao();
       			cidade.setNomeMesorregiao(mesoregiao.getNome());
       			cidade.setNomeFormatado(cidade.getNomeCidade() + "/" + cidade.getSiglaEstado());
       			cidades.addCidades(cidade);
       		}); 
       		
       	});     
       	try {
			PrintWriter writer = response.getWriter();
		    StatefulBeanToCsv sbc = new StatefulBeanToCsvBuilder(writer)
		    	       .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
		    	       .build();
		    sbc.write(cidades.getCidades());
		    writer.close();
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Erro convertendo arquivo CSV:" + e.getMessage());
		} catch (CsvDataTypeMismatchException e) {
			logger.error("Erro convertendo arquivo CSV:" + e.getMessage());
		} catch (CsvRequiredFieldEmptyException e) {
			logger.error("Erro convertendo arquivo CSV:" + e.getMessage());
		}
       	return null;
    }      
	
	// Ler Cidade
	@GetMapping(path = "/cidade/{nomeCidade}", produces = "application/json; charset=UTF-8")
	public CidadeID getCidade(@PathVariable String nomeCidade) {
		logger.debug("nomeCidade=" + nomeCidade);
		CidadeID cidadeid = new CidadeID();
		Municipio municipio =  localidadeService.getMunicipio(nomeCidade);
		if (municipio != null) {
			cidadeid.setId(municipio.getId());
		} 	
		return cidadeid;
	}  

    // Lista Estados
    @GetMapping(path = "/estados", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public List<Estado>  listEstados() { 
    	return localidadeService.listEstados();
    }  
       
    // Lista Cidades
    @GetMapping(path = "/cidades/{UF}", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public List<Municipio> listCidadesbyUF(@PathVariable String UF) {    
    		logger.debug("UF=" + UF);
       		List<Municipio> municipios = localidadeService.listMunicipios(UF);
		return municipios;
    }  
       
	
}