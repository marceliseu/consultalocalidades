package br.com.marceliseu.ConsultaLocalidades;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import br.com.marceliseu.model.Municipio;
import br.com.marceliseu.service.LocalidadeService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class LocalidadeControllerIntegrationTest  {

	Logger logger = LoggerFactory.getLogger(LocalidadeControllerIntegrationTest.class);
	
    @Autowired
    private MockMvc mvc;
    @Autowired
	private LocalidadeService localidadeService;
 
    boolean posts_inicializados = false;

    @Before
	public void inicializa() throws Exception {
		logger.info("inicializando...");
 
		logger.info("inicializado");

	}

	
    @Test
    public void verificaEstados() throws Exception
    {
      this.mvc.perform( MockMvcRequestBuilders
          .get("/estados")
          .accept(MediaType.APPLICATION_JSON))
      	  .andDo(print())
      	  .andExpect(status().isOk())
      	  .andExpect(jsonPath("$", hasSize(27))); 
    }
    
    @Test
    public void verificaCidadeID() throws Exception
    {
      this.mvc.perform( MockMvcRequestBuilders
          .get("/cidade/Florianopolis")
          .accept(MediaType.APPLICATION_JSON))
      	  .andDo(print())
      	  .andExpect(status().isOk())
      	  .andExpect(jsonPath("$.id", is(4205407)));    
    }    
    
    
    @Test
    public void listaCidades() throws Exception
    {
      List<Municipio> municipios = localidadeService.listMunicipios("42");
      assertEquals(295, municipios.size());
    }
       
}
