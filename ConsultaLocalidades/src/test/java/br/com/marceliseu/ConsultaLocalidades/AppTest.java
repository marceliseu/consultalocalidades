package br.com.marceliseu.ConsultaLocalidades;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.junit.Test;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.marceliseu.model.Cidade;
import br.com.marceliseu.model.CidadeList;
import br.com.marceliseu.model.Mesorregiao;
import br.com.marceliseu.model.Microrregiao;
import br.com.marceliseu.model.Municipio;
import br.com.marceliseu.model.Regiao;
import br.com.marceliseu.model.UF;



class ShortFormatter extends Formatter {
    public String format(LogRecord rec) {
    		String msg ;
    		msg = calcDate(rec.getMillis()) + " " + 
    		rec.getSourceMethodName() + ":" + 
    		rec.getMessage() + "\n";;
		return msg;
    }
    private String calcDate(long millisecs) {
   	 SimpleDateFormat date_format = new SimpleDateFormat("dd/MM/yy HH:mm");
       Date resultdate = new Date(millisecs);
       return date_format.format(resultdate);
   }
}

public class AppTest {
	
	private final static Logger logger = Logger.getLogger(AppTest.class.getName());
	static private Formatter formatter  = new ShortFormatter();
	
	public AppTest() {
		Logger rootLog = Logger.getLogger("");
		rootLog.setLevel( Level.INFO );
		Handler rootHandler = rootLog.getHandlers()[0];
		rootHandler.setLevel( Level.INFO );   // Default console handler
		rootHandler.setFormatter(formatter);
		
		logger.info("Inicializado " );
	}

	@Test
	public void TestaObjPost()
	{
		CidadeList cidadelist = new CidadeList();
		Cidade cidade = new Cidade();
		cidade.setIdEstado(4205407);
		cidade.setNomeCidade("Florianóplis");
		cidade.setNomeFormatado(cidade.getSiglaEstado() + "/" + cidade.getNomeCidade());
		
		Regiao regiao = new Regiao();
		regiao.setId(4);
		regiao.setNome("Sul");
		regiao.setSigla("S");
		UF uf = new UF();
		uf.setId(42);
		uf.setNome("Santa Catarina");
		uf.setRegiao(regiao);
		Mesorregiao mesorregiao = new Mesorregiao();
		mesorregiao.setId(4205);
		mesorregiao.setNome("Grande Florianópolis");
		mesorregiao.setUF(uf);
		Microrregiao microregiao = new Microrregiao();
		microregiao.setId(42016);
		microregiao.setNome("Florianópolis");
		microregiao.setMesorregiao(mesorregiao);
		Municipio municipio = new Municipio();
		municipio.setId(4205407);
		municipio.setNome("Florianóplis");
		municipio.setMicrorregiao(microregiao);

		cidadelist.addCidades(cidade);
		logger.info("municipio:" + municipio.getNome());
		logger.info("id municipio:" + municipio.getId());
		assertEquals(Integer.valueOf(4205407), municipio.getId());
	}
	
	@Test
    public void verificaMunicipiosJSON() {
    	List<Municipio> municipios = null;
        try {
             InputStream inJson = AppTest.class.getResourceAsStream("/municipios.json");
             ObjectMapper mapper = new ObjectMapper();
             try {
            	 municipios = mapper.readValue(
            			 inJson, new TypeReference<List<Municipio>>() { });
            	 logger.info("Total municipios:" + new Integer(municipios.size()).toString());
            	 assertEquals(295, municipios.size());
            	 Municipio muncipio1 = municipios.get(0);
            	 logger.info(muncipio1.getNome());
            	 logger.info(muncipio1.getMicrorregiao().getNome());
            	 logger.info(muncipio1.getMicrorregiao().getMesorregiao().getNome());
            	 UF uf = muncipio1.getMicrorregiao().getMesorregiao().getUF();
            	 logger.info("UF:" + uf.getNome());
             } catch (JsonParseException e) {
                 e.printStackTrace();
             } catch (JsonMappingException e) {
                 e.printStackTrace();
             } catch (IOException e) {
                 e.printStackTrace();
             }             
     
        } catch (RestClientException e) {
            e.printStackTrace();
        }

    }
}
